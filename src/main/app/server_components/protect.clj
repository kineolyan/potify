(ns app.server-components.protect
  (:import [javax.crypto.spec SecretKeySpec PBEKeySpec]
           [java.nio.charset StandardCharsets]
           [javax.crypto Cipher SecretKeyFactory]
           [java.util Base64]))

;;; Basic base64 methods

(defn ->bytes
  [s]
  (.getBytes s StandardCharsets/UTF_8))

(defn ->base64
  [s]
  (let [bytes (cond
                (string? s) (->bytes s)
                (bytes? s) s)
        encoder (Base64/getEncoder)]
    (.encodeToString encoder bytes)))

(defn base64->
  [s & {:keys [output] :or {output :str}}]
  (let [decoder (Base64/getDecoder)
        decoded-bytes (.decode decoder s)]
    (case output
      :str (String. decoded-bytes)
      :bytes decoded-bytes)))

(comment
  (->base64 "abcde")
  (base64-> *1)
  (base64-> "YWJj" :output :bytes)
  (base64-> "YWJj" :output :wat))

;;; Encoding with secret

(def -secret-word (System/getenv "POTIFY_KEY"))
(def secret-key
  ; could be extracted to a function
  (let [salt (->bytes "this is the end.")
        _ (assert (= (count salt) 16) "Salt must contains 16 bytes")
        spec (PBEKeySpec. (.toCharArray -secret-word) salt 1000000 256)
        f (SecretKeyFactory/getInstance "PBKDF2WithHmacSHA1")
        key (-> f (.generateSecret spec) (.getEncoded))]
    (SecretKeySpec. key "AES")))

(defn cipher
  [mode key bs]
  (let [cipher (Cipher/getInstance "AES")
        _ (.init cipher mode key)]
    (.doFinal cipher bs)))

(defn encode
  ([value] (encode secret-key value))
  ([key value]
   (let [encrypted-bytes (cipher Cipher/ENCRYPT_MODE key (->bytes value))]
     (->base64 encrypted-bytes))))

(comment
  (doto (Cipher/getInstance "AES")
    (.init Cipher/ENCRYPT_MODE secret-key)))

(defn decode
  ([encrypted-value] (decode secret-key encrypted-value))
  ([key encrypted-value]
   (let [encrypted-bytes (base64-> encrypted-value :output :bytes)
         decrypted-bytes (cipher Cipher/DECRYPT_MODE key encrypted-bytes)]
     (String. decrypted-bytes))))

(comment
  (encode secret-key "do not share to anyone")
  (def coded-value "fM1QQdVbh7XnBJWN1t56HU6Ob4xG9an+k5O6SSAcrbE=")
  (decode secret-key *1)
  (decode secret-key coded-value))
