(ns app.server-components.config
  (:require [clojure.java.io :as io]
            [mount.core :refer [defstate args]]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]))

(defn configure-logging! [config]
  (let [{:keys [taoensso.timbre/logging-config]} config]
    (log/info "Configuring Timbre with " logging-config)
    (log/merge-config! logging-config)))

(defn load-config-file!
  [path]
  (->>
   (io/resource path)
   slurp
   edn/read-string))

(defn load-config!
  [options]
  (let [default-config (load-config-file! "config/defaults.edn")
        overrides (when-let  [override-file (:config-path options)]
                    (load-config-file! override-file))]
    (merge default-config overrides)))

(defstate config
  :start (let [{:keys [config] :or {config "config/dev.edn"}} (args)
               configuration (load-config! {:config-path config})]
           (log/info "Loaded config" config)
           (configure-logging! configuration)
           configuration))
