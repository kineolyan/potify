(ns app.server-components.middleware
  (:require [mount.core :refer [defstate]]
            [ring.middleware.defaults :refer [wrap-defaults]]
            [ring.util.response :as resp]
            [hiccup.core]
            [hiccup.page]
            [steffan-westcott.clj-otel.api.trace.http :as trace-http]
            [app.server-components.config :refer [config]]
            [app.server-components.router :as router]))

(def ^:private req->not-found
  (fn [_]
    {:status  404
     :headers {"Content-Type" "text/plain"}
     :body    "NOPE"}))

(defn not-found-handler
  [handler]
  (fn not-found-fn
    [req]
    (or (handler req)
        (req->not-found req))))

(defn hiccup->html
  [response]
  (case (:content-type response)
    :hiccup/page
    (resp/content-type
     (update response :body #(hiccup.page/html5 %))
     "text/html")

    :hiccup/fragment
    (resp/content-type
     (update response :body #(hiccup.core/html %))
     "text/html")

    response))

(defn hiccup->html-handler
  [handler]
  (fn [req]
    (-> req handler hiccup->html)))

(defstate middleware
  :start
  (let [defaults-config (:ring.middleware/defaults-config config)
        legal-origins   (get config :legal-origins #{"localhost"})]
    (-> #'router/router
        hiccup->html-handler
        not-found-handler
        (trace-http/wrap-server-span {:create-span? true})
        (wrap-defaults defaults-config))))
