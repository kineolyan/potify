(ns app.server-components.router
  (:require [reitit.ring :as rtt]
            [reitit.coercion.spec]
            [reitit.ring.coercion :as rrc]
            [reitit.ring.middleware.parameters :as parameters]
            [ring.util.response :as resp]
            [app.model.mock-database :refer [conn]]
            [app.ops.actions :as actions :refer [reports]]
            [app.server-components.spotify :as spotify]
            [app.server-components.pages :as pages]))

(defn spotify-login
  [_]
  (spotify/generate-spotify-auth-url))

(defn spotify-callback!
  [req]
  (let [code (-> req :query-params (get "code"))
        _ (swap! spotify/secret-db assoc :code code)
        secrets (spotify/get-token! code)
        _ (swap! spotify/secret-db merge secrets)]
    (format "logged in\ntoken = %s" (:token secrets))))

(defn req->root
  [{:keys [anti-forgery-token]}]
  (-> (resp/response (pages/index anti-forgery-token))
      (resp/content-type "text/html")))

; (defn respond-to-view
;   [req]
;   (let [session (:session req)
;         id (some-> req :path-params :id Integer/parseInt)
;         reset? (-> req :query-params (contains? "reset-search"))]
;     (merge
;      {:content-type :hiccup/page
;       :body (views/build-index {:pokemon id :csrf "none"})}
;      (when reset?
;        {:session (dissoc session :search)}))))

(defn req->version
  [_]
      (-> (resp/response (pages/version))
          (resp/content-type "text/html")))

(defn req->health
  [_]
      (-> (resp/response "OK")
          (resp/content-type "text/plain")))

(defn req->report-index
  [{:keys [anti-forgery-token]}]
      (-> (resp/response (pages/build-report @reports anti-forgery-token))
          (resp/content-type "text/html")))

(defn req->report-trigger
  [_]
      (-> (resp/response (pages/trigger-reports conn reports))
          (resp/content-type "text/html")))

(defn req->report-refresh
  [_]
      (-> (resp/response (pages/refresh-report @reports))
          (resp/content-type "text/html")))

(defn req->spotify-login
    [{:keys [anti-forgery-token]}]
      (-> (resp/redirect (spotify-login anti-forgery-token))))

(defn req->spotify-callback
  [req]
      (-> (resp/response (spotify-callback! req))
          (resp/content-type "text/plain")))

(def router
  (rtt/ring-handler
   (rtt/router
    [["/" {:get {:handler #'req->root}}]
     ["/index.html" {:get {:handler #'req->root}}]
     ["/version.html" {:get {:handler #'req->version}}]
     ["/health" {:get {:handler #'req->health}}]
     ["/report"
      ["/index.html" {:get {:handler #'req->report-index}}]
      ["/trigger" {:post {:handler #'req->report-trigger}}]
      ["/refresh" {:get {:handler #'req->report-refresh}}]]
     ["/login-to-spotify" {:get {:handler #'req->spotify-login}}]
     [spotify/redirect-endpoint {:post {:handler #'req->spotify-callback}}]]
    {:data {:coercion   reitit.coercion.spec/coercion
            :middleware [parameters/parameters-middleware
                         rrc/coerce-request-middleware]}})))

(comment
    (router {:uri "/"
             :request-method :get}))
