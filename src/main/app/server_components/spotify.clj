(ns app.server-components.spotify
  (:require
   [clojure.string :as str]
   [clojure.pprint :as pp]
   [clj-http.client :as client]
   [cheshire.core :as json]
   [ring.util.codec :as codec]
   [app.server-components.protect :as protect]
   [app.model.mock-database :as db]
   [datascript.core :as d]
   [clj-spotify.core :as sptfy]
   [app.model.config :as cfg]
   [taoensso.timbre :as log])
  (:import [java.time Instant]))

;;; Configuration

(def redirect-endpoint "/interact")

(def app-info
  {:id (System/getenv "SPOTIFY_CLIENT")
   :secret (System/getenv "SPOTIFY_SECRET")
   :redirect-uri (str "http://localhost:3000" redirect-endpoint)})

(defonce secret-db (atom {}))
(comment
  (reset! secret-db {}))

(def scopes
  ["user-read-private"
   "user-read-email"

   "playlist-read-private"
   "playlist-read-collaborative"
   "playlist-modify-private"
   "playlist-modify-public"

   "user-library-modify"
   "user-library-read"])

;;; Authentication

(defn random-string [length]
  (apply str (repeatedly length #(rand-nth "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"))))

(comment
  (codec/form-encode {:a "a b c" :b 1}))

(defn generate-spotify-auth-url
  []
  (let [state (random-string 16)
        {:keys [id secret redirect-uri]} app-info]
    (str
     "https://accounts.spotify.com/authorize?"
     (codec/form-encode {:response_type "code"
                         :state state
                         :scope (str/join " " scopes)
                         :redirect_uri redirect-uri
                         :client_id id
                         :client_secret secret}))))

(defn get-token!
  [code]
  (let [{:keys [id secret :redirect-uri]} app-info
        response (client/post
                  "https://accounts.spotify.com/api/token"
                  {:form-params {:code code
                                 :redirect_uri redirect-uri
                                 :grant_type "authorization_code"}
                   :headers {"Authorization" (str "Basic " (protect/->base64 (str id ":" secret)))}})
        body (-> response :body (json/decode))]
    {:token (get body "access_token")
     :refresh (get body "refresh_token")
     :expiration (+ (System/currentTimeMillis) (* 1000 (get body "expires_in")))}))

(defn refresh-token!
  [code]
  (let [{:keys [id secret]} app-info
        response (client/post
                  "https://accounts.spotify.com/api/token"
                  {:form-params {:refresh_token code
                                 :grant_type "refresh_token"}
                   :headers {"Authorization" (str "Basic " (protect/->base64 (str id ":" secret)))}})
        body (-> response :body (json/decode))]
    {:token (get body "access_token")
     :refresh (get body "refresh_token" code)
     :expiration (+ (System/currentTimeMillis) (* 1000 (get body "expires_in")))}))

(def get-token-query
  '[:find (pull ?e [:account/code :account/token :account/expiration])
    :in $ ?id
    :where
    [?e :account/id ?id]])

(defn will-expire?
  "Computes if the given timestamp will expire within the next minute"
  ([timestamp] (will-expire? timestamp (System/currentTimeMillis)))
  ([timestamp current-time]
   (let [limit (+ current-time 60000)]
     (< timestamp limit))))

(defn get-token-from-db
  "Reads the token from the database, reporting if the token needs to be refresh or
  if the user must login."
  [db user]
  (let [{:account/keys [code token expiration]} (ffirst (d/q get-token-query db user))]
    (cond
      (and token expiration (not (will-expire? expiration)))
      [:valid (protect/decode token)]
      code
      [:refresh (protect/decode code)]
      :else
      [:login])))

(comment
  (get-token-from-db @db/conn "me"))

(defn get-refresh-token
  [db user]
  (let [result (d/pull db [:account/code] [:account/id user])]
    (some-> result :account/code protect/decode)))

(defn build-secret-db
  [{:keys [db user]}]
  (when-let [code (get-refresh-token db user)]
    (refresh-token! code)))

(defn save-token-to-db!
  [connection user {:keys [token refresh expiration]}]
  (d/transact!
   connection
   [#:account{:id user
              :code (protect/encode refresh)
              :token (protect/encode token)
              :expiration expiration}])
  nil)

(comment

  ; load a token from the code in the database
  (get-token-from-db @db/conn "kineolyan@proton.me")
  (get-refresh-token @db/conn "kineolyan@proton.me")
  (refresh-token! (:refresh @secret-db))
  (let [user "kineolyan@proton.me"
        result (build-secret-db {:db @db/conn :user user})]
    (save-token-to-db! db/conn user result)))

;;; Functions of the API

(defn -call
  [f & args]
  (let [result (apply f args)]
    (tap> result)
    (if-let [e (:error result)]
      (throw (ex-info
              (or (:message e) "Error")
              e))
      result)))

(defn -collect-playlist-details
  [playlist]
  (as-> (select-keys playlist [:id :name]) m
    (assoc m
           :owner (get-in playlist [:owner :id])
           :snapshot-id (:snapshot_id playlist)
           :track-count (get-in playlist [:tracks :total]))
    (merge m (when (str/starts-with? (:name m) "[p] ")
               {:generated true}))))

(defn collect-playlists!
  [token user]
  (loop [items []
         offset 0]
    (let [limit 40
          result (-call
                  sptfy/get-a-list-of-a-users-playlists
                  {:user_id user :limit limit :offset offset}
                  token)
          items (->> (:items result)
                     (keep identity)
                     (map -collect-playlist-details)
                     (concat items))]
      (if-not (:next result)
        (vec items)
        (recur items (+ offset limit))))))

(defn -collect-track-details
  [{:keys [track added_at]}]
  {:id (-> track :id)
   :uri (-> track :uri)
   :name (-> track :name)
   :added (-> added_at Instant/parse .toEpochMilli)
   :album (-> track :album :name)})

(defn collect-tracks!
  "Collects all the tracks for the given playlist, referenced by its id."
  [token playlist-id]
  (loop [items []
         offset 0]
    (let [limit 40
          result (-call
                  sptfy/get-a-playlists-tracks
                  {:limit limit
                   :offset offset
                   :playlist_id playlist-id}
                  token)
          items (concat items (map -collect-track-details (:items result)))]
      (if-not (:next result)
        (vec items)
        (recur items (+ offset limit))))))

(defn collect-library!
  "Collects the entire library of the user behind the token."
  [token]
  (loop [items []
         offset 0]
    (let [limit 40
          result (-call
                  sptfy/get-users-saved-tracks
                  {:limit limit
                   :offset offset}
                  token)
          items (concat items (map -collect-track-details (:items result)))]
      (if-not (:next result)
        (vec items)
        (recur items (+ offset limit))))))

(comment
  ; testing something with the clj-spotify
  (def user-playlists-response
    (sptfy/get-a-list-of-current-users-playlists
     {:limit 20}
     (:token @secret-db)))
  (def playlists
    (time (collect-playlists! token "kineolyan")))
  (pp/print-table [:id :name :snapshot_id] playlists)
  (count playlists)
  (keys (first playlists))

  ; loading tracks information
  (def playlist-tracks
    (sptfy/get-a-playlists-tracks
     {:limit 50
      :offset 0
      :playlist_id "6GWQo5Vfp5XJYVHbnPnxPh"}
     token))
  (def some-tracks
    (time (collect-tracks!
           token
           "6GWQo5Vfp5XJYVHbnPnxPh")))
  (-collect-track-details (first some-tracks))
  (pp/print-table some-tracks)

  ; library
  (def library
    (time (collect-library! (:token @secret-db)))))

;;; Database feeding

(defn get-valid-token!
  "Gets a valid token, refreshing it if necessary"
  [connection user]
  (let [[result value] (get-token-from-db @connection user)]
    (case result
      :valid
      value
      :refresh
      (let [new-details (refresh-token! value)
            _ (save-token-to-db! connection user new-details)
            [new-result token] (get-token-from-db @connection user)]
        (if (= new-result :valid)
          token
          (throw (ex-info "Failed to refresh token" {:user user
                                                     :status new-result}))))
      :login
      (throw (ex-info "Unlogged user" {:user user})))))

(defn save-playlists!
  "Saves all the playlist details owned by the passed user to the database."
  [{:keys [connection user token all?]}]
  (let [token (or token (get-valid-token! connection user))
        spotify-user (cfg/get-spotify-account @connection user)
        playlists (collect-playlists! token spotify-user)
        transaction (for [{:keys [name generated owner] :as playlist} playlists
                          :when (or all? (= owner spotify-user))]
                      (let [{:keys [id snapshot-id track-count]} playlist]
                        (merge
                         #:playlist{:id id
                                    :name name
                                    :snapshot-id snapshot-id
                                    :track-count track-count
                                    :owner [:account/id user]}
                         (when generated {:playlist/generated true}))))]
    (d/transact!
     connection
     transaction))
  nil)

(defn get-recorded-playlists
  [db owner]
  (d/q
   '[:find ?e ?name
     :in $ ?u
     :where
     [?uid :account/id ?u]
     [?e :playlist/owner ?uid]
     [?e :playlist/name ?name]]
   db
   owner))

(defn playlist-exists?
  [db owner name]
  (let [result (d/q '[:find ?e :in $ ?o ?n
                      :where
                      [?e :playlist/name ?n]
                      [?e :playlist/owner ?oid]
                      [?oid :account/id ?o]]
                    db owner name)]
    (boolean (seq result))))

(defn get-playlist-id
  "Gets the Spotify ID of a playlist, given its name and owner."
  [db owner playlist-name]
  (let [result (d/q '[:find ?id
                      :in $ ?o ?n
                      :where
                      [?oid :account/id ?o]
                      [?e :playlist/owner ?oid]
                      [?e :playlist/name ?n]
                      [?e :playlist/id ?id]]
                    db owner playlist-name)]
    (ffirst result)))

(defn create-auto-playlist!
  "Creates a playlist for the given user.
  If the playlist is **already recorded in the database**, nothing is done.
  This may create duplicates if the database is not up-to-date."
  [{:keys [connection user token name]}]
  {:pre [(some? name)]}
  (let [playlist-name (str "[p] " name)]
    (if (playlist-exists? @connection user playlist-name)
      {:name playlist-name
       :id (get-playlist-id @connection user playlist-name)
       :existing? true}
      (let [token (or token (get-valid-token! connection user))
            {:keys [id :snapshot_id]} (-call
                                       sptfy/create-a-playlist
                                       {:user_id (cfg/get-spotify-account @connection user)
                                        :name playlist-name
                                        :collaborative false
                                        :public false}
                                       token)]
        (d/transact!
         connection
         [#:playlist{:id id
                     :name playlist-name
                     :snapshot-id snapshot_id
                     :owner [:account/id user]
                     :generated true}])
        {:name playlist-name
         :id id
         :created? true}))))

(defn get-playlist-songs
  "Gets the IDs of the tracks in the playlist, in the playlist order."
  [db owner playlist-name]
  (when-let [playlist-id (get-playlist-id db owner playlist-name)]
    (:playlist/songs (d/entity db [:playlist/id playlist-id]))))

(defn -get-song-info
  [db song-id]
  (d/pull db [:song/name :song/album] [:song/id song-id]))

(defn save-tracks!
  "Save all tracks for the given playlist into the database."
  [{:keys [connection user token playlist-name]}]
  (if-let [playlist-id (get-playlist-id @connection user playlist-name)]
    (let [token (or token (get-valid-token! connection user))
          songs (map
                 #(assoc %1 :tid (- (+ 1000 %2)))
                 (collect-tracks! token playlist-id)
                 (range))
          song-tx (for [{:keys [id name album tid uri]} songs]
                    {:db/id tid
                     :song/id id
                     :song/name name :song/album album :song/uri uri})
          playlist-tx (for [{:keys [tid]} songs]
                        #:playlist-song{:song tid :playlist -1})
          playlist-update-tx {:db/id -1 ; not to have to find the correct id from name+owner
                              :playlist/id playlist-id
                              :playlist/songs (mapv :id songs)}]
      (d/transact!
       connection
       (concat
        song-tx
        playlist-tx
        [playlist-update-tx]))
      :saved)
    :unknown-playlist))

(defn -empty-playlist-in-spotify!
  [token playlist-id]
  (let [songs (collect-tracks! token playlist-id)
        uris (map :uri songs)]
    (doseq [uris (partition 100 100 nil uris)]
      (-call
       sptfy/remove-tracks-from-a-playlist
       {:playlist_id playlist-id
        :tracks (map #(hash-map :uri %1) uris)}
       token))))

(defn -empty-playlist-in-db!
  [connection playlist-id]
  (let [result (d/q
                '[:find ?e :in $ ?id
                  :where
                  [?pid :playlist/id ?id]
                  [?e :playlist-song/playlist ?pid]]
                @connection playlist-id)
        eids (map first result)]
    (db/retract-entities! connection eids)
    (d/transact!
     connection
     [#:playlist{:id playlist-id
                 :track-count 0}])
    nil))

(defn empty-playlist!
  [{:keys [connection user token playlist-name]}]
  (let [token (or token (get-valid-token! connection user))
        db @connection
        playlist-id (get-playlist-id db user playlist-name)]
    (-empty-playlist-in-spotify! token playlist-id)
    (-empty-playlist-in-db! connection playlist-id)))

(defn append-to-playlist!
  [{:keys [connection user playlist-name song-ids song-uris]}]
  (let [token (get-valid-token! connection user)
        db @connection
        spotify-account (cfg/get-spotify-account db user)
        playlist-id (get-playlist-id db user playlist-name)
        uris (or song-uris
                 (map #(:song/uri (d/entity db [:song/id %])) song-ids))]
    (doseq [uris (partition 100 100 nil uris)
            :let [uris (remove nil? uris)]]
      (-call
       sptfy/add-tracks-to-a-playlist
       {:user_id spotify-account
        :playlist_id playlist-id
        :uris uris}
       token))))

(comment
  ;; operations
  (def token (get-valid-token! db/conn db/olivier))
  (def payload-oli {:connection db/conn
                    :user db/olivier})

   ; check that the token is working
  (sptfy/get-current-users-profile {:user-id "kineolyan"} token)

  (create-auto-playlist!
   {:connection db/conn
    :user db/olivier
    :name "testing potify"})
  ; remove the created playlist manually
  (db/retract-entities! db/conn [8])

  (save-tracks! {:connection db/conn :user db/olivier
                 :playlist-name "2023"})
  (:playlist/songs (d/entity @db/conn 7))
  (:song/name (d/entity @db/conn [:song/id "0osif2HfdypCQOL7F6Jsiv"]))

  (let [db @db/conn
        user db/olivier
        songs (:playlist/songs (d/entity db 7))
        songs (take 5 songs)
        uris (map #(:song/uri (d/entity db [:song/id %])) songs)
        spotify-account (:account/spotify (d/entity db [:account/id user]))
        playlist-id (get-playlist-id db user "[p] testing potify")]
    (doseq [uris (partition 100 100 nil uris)]
      (-call
       sptfy/add-tracks-to-a-playlist
       {:user_id spotify-account
        :playlist_id playlist-id
        :uris uris}
       token)))
  (empty-playlist! {:connection db/conn
                    :user db/olivier
                    :playlist-name "[p] testing potify"})

  ;; some tests to check the fns
  (sptfy/get-current-users-profile
   {}
   (get-valid-token! db/conn db/colombe))

  ;; a call that will fail
  (-call
   sptfy/get-current-users-profile
   {}
   "not-a-token")

  (take 3 (collect-playlists! token "chtitecerise")))
