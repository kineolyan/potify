(ns app.server-components.pages
  (:require [app.server-components.config :refer [config]]
            [app.model.mock-database :refer [conn]]
            [app.ops.actions :as actions :refer [reports]]
            [mount.core :refer [defstate]]
            [ring.middleware.defaults :refer [wrap-defaults]]
            [ring.util.response :as resp]
            [steffan-westcott.clj-otel.api.trace.http :as trace-http]
            [hiccup.page :refer [html5]]
            [taoensso.timbre :as log]
            [app.server-components.spotify :as spotify]))

(defn index [csrf-token]
  (log/debug "Serving index.html")
  (html5
   [:html {:lang "en"}
    [:head {:lang "en"}
     [:title "Potify"]
     [:meta {:charset "utf-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"}]
     [:link {:rel "shortcut icon" :href "data:image/x-icon;," :type "image/x-icon"}]]
    [:body
     [:div#app "App - building in progress"]]]))

(defn version []
  (html5
   [:html {:lang "en"}
    [:head {:lang "en"}
     [:title "Potify"]
     [:meta {:charset "utf-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"}]
     [:link {:rel "shortcut icon" :href "data:image/x-icon;," :type "image/x-icon"}]]
    [:body
     [:p "Version 0.0.2"]]]))

(defn -generate-step-progresses
  [report]
  (when-let [operations (seq (:progress report))]
    [:ul
     (map
      (fn [[k v]] [:li {:key k} [:b k] ": " [:span.is-family-monospace.tag.is-info.is-light v]])
      operations)]))

(defn -generate-report
  [report]
  (let [{:keys [running?]} report]
    [:div#report
     [:p "Update in progress: " (if running? "yes" "no")]
     (when-let [{:keys [timestamp]} report]
       [:p
        [:i "Last update at " [:span.tag.is-primary.is-light (str timestamp)]]])
     (-generate-step-progresses report)
     [:button.button.is-link
      {:hx-post "/report/trigger"
       :hx-trigger "click"
       :hx-target "#report"
       :hx-swap "outerHTML"}
      "Trigger update"]]))

(defn build-report
  [report anti-forgery-token]
  (html5
   [:html {:lang "en"}
    [:head {:lang "en"}
     [:title "Potify"]
     [:meta {:charset "utf-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"}]
     [:link {:href "https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css"
             :rel  "stylesheet"}]
     [:link {:rel "shortcut icon" :href "data:image/x-icon;," :type "image/x-icon"}]]
    [:body
     [:section.section
      [:div.container
       [:h1.title "Recipe execution"]
       (-generate-report report)]]
     [:script {:src "https://unpkg.com/htmx.org@1.9.10"
               :integrity "sha384-D1Kt99CQMDuVetoL1lrYwg5t+9QdHe7NLX/SoJYkXDFfX37iInKRy5xLSi8nO7UC"
               :crossorigin "anonymous"}]
     [:script (format
               "document.body.addEventListener('htmx:configRequest', (event) => { event.detail.headers['X-CSRF-Token'] = '%s'; })"
               anti-forgery-token)]]]))

(defn -generate-report-progress
  [report]
  [:div#report-status
   {:hx-get "/report/refresh"
    :hx-target "#report-status"
    :hx-swap "outerHTML"
    :hx-trigger "every 2s"}
   [:p "Update in progress"]
   (-generate-step-progresses report)
   [:button.button.primary.is-light
    {:hx-get "/report/refresh"
     :hx-trigger "click"
     :hx-target "#report-status"
     :hx-swap "outerHTML"}
    "Refresh status"]])

(comment
  (-generate-report-progress @reports))

(defn trigger-reports
  [connection reports]
  (actions/execute-and-update!
   {:reports reports
    :connection connection})
  (html5
   (-generate-report-progress @reports)))

(defn refresh-report
  [report]
  (if (:running? report)
    (html5 (-generate-report-progress report))
    (html5 (-generate-report report))))
