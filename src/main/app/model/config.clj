(ns app.model.config
  (:require [datascript.core :as d]
            [app.model.mock-database :as db]))

(defn get-users
  [db]
  (as->
   (d/q '[:find ?u
          :where [_ :account/id ?u]]
        db)
   result
    (map first result)))

(defn get-time-playlists
  [db user]
  (let [result (d/q
                '[:find (pull ?c [:config/time-playlists])
                  :in $ ?u
                  :where
                  [?e :account/id ?u]
                  [?c :config/user ?e]]
                db
                user)]
    (when (seq result)
      (:config/time-playlists (ffirst result)))))

(defn get-spotify-account
  [db user]
  (:account/spotify (d/entity db [:account/id user])))

(comment
  (get-time-playlists
   @db/conn
   db/colombe)
  (get-spotify-account @db/conn db/olivier))
