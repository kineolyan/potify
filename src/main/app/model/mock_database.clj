(ns app.model.mock-database
  (:require [clojure.edn :as edn]
            [datascript.core :as d]
            [mount.core :refer [defstate]]
            [datascript.storage]
            [datascript.storage.sql.core :as storage-sql]
            [lib.kineolyan.storage-turso :as turso])
  (:import [java.sql DriverManager]
           [javax.sql DataSource]))

;; In datascript just about the only thing that needs schema
;; is lookup refs and entity refs.  You can just wing it on
;; everything else.
(def schema {:account/id {:db/cardinality :db.cardinality/one
                          :db/unique      :db.unique/identity}
             :playlist/id {:db/cardinality :db.cardinality/one
                           :db/unique :db.unique/identity}
             :playlist/owner {:db/valueType :db.type/ref}
             :playlist/song {:db/cardinality :db.cardinality/many
                             :db/valueType :db.type/ref}
             :config/user {:db/valueType :db.type/ref
                           :db/unique :db.unique/identity}
             :song/id {:db/cardinality :db.cardinality/one
                       :db/unique :db.unique/identity}
             :playlist-song/song {:db/valueType :db.type/ref
                                  :db/isComponent true}
             :playlist-song/playlist {:db/valueType :db.type/ref
                                      :db/isComponent true}})

(defn new-database [] (d/create-conn schema))

(defn read-surely
  "Reads a value, with a deref if needed."
  ;; TODO this is a bit of a hack
  [v]
  (if (instance? clojure.lang.IDeref v)
    (deref v)
    v))

(defn create-storage
  [config]
  (tap> [:create-store config])
  (when (:remote? (read-surely config))
    (case (:type config)
      :jdbc
      (let [_ (Class/forName "com.mysql.cj.jdbc.Driver")
            datasource (fn datasource [connect-fn]
                         (reify DataSource (getConnection [_] (connect-fn))))
            connect-fn #(DriverManager/getConnection
                         (format "jdbc:mysql://%s/%s%s"
                                 (System/getenv "DB_HOST")
                                 (System/getenv "DB_NAME")
                                 (or (some->> (System/getenv "DB_OPTIONS") (str "?"))
                                     ""))
                         (System/getenv "DB_USERNAME")
                         (System/getenv "DB_PASSWORD"))]
        (storage-sql/make
         (datasource connect-fn)
         {:dbtype :mysql}))

      :turso
      (let [opts (turso/env->options)
            complete-opts (assoc opts :table-name "potify")]
        (turso/create-storage complete-opts)))))

(defn restore-sql-database
  [store]
  (let [store (read-surely store)]
    (d/restore-conn store)))

(defn create-database
  [config store]
  (tap> [:create-db config store])
  (if (:remote? (read-surely config))
    (restore-sql-database (read-surely store))
    (new-database)))

; (defstate config :start {:remote? true})
; (defstate store :start (create-storage config))
; (defstate conn :start (create-database config store))

(def config {:remote? true
             :type :turso})
(defn create-database-bis
  []
  (let [store (create-storage config)]
    (create-database config store)))

(defstate conn :start (create-database-bis))

(defn retract-entities!
  "Retracts the passed eids from the database in a single transaction."
  [connection eids]
  (let [entries (d/q '[:find ?e ?a ?v
                       :in $ [?e ...]
                       :where [?e ?a ?v]]
                     @connection eids)]
    (d/transact!
     connection
     (map #(into [:db/retract] %) entries))))

(def olivier "kineolyan@proton.me")
(def colombe "colomberib@gmail.com")

(comment
  (d/transact!
   conn
   [{:account/id olivier
     :account/name "Kineolyan"
     :account/real-name "Olivier"}])
  (d/q
   '[:find ?a
     :where [?n :account/id olivier]
     [?n :account/name ?a]]
   @conn)
  (d/pull
   @conn
   [:account/name :account/real-name :account/spotify]
   [:account/id olivier])
  (d/transact!
   conn
   [{:account/id colombe
     :account/real-name "Colombe"}])
  ; List all users in the database
  (d/q
   '[:find ?n ?s
     :where
     [?e :account/real-name ?n]
     [?e :account/spotify ?s]]
   @conn))

(defn save-db
  [db file-path]
  (spit file-path (pr-str db) :encoding "UTF-8"))

(defn load-db
  [file-path]
  (edn/read-string
   {:readers d/data-readers}
   (slurp file-path)))

(defn -migrate-databases
  "Migrates the data from one database to another.
  This will read the whole data set from the source and make one large transaction on the new one"
  [old-conn new-conn]
  (d/transact!
   new-conn
   (->> (d/datoms @old-conn :eavt)
        (map #(take 3 %))
        (map #(into [:db/add] %)))))

(comment
  (let [c (new-database)]
    (d/transact!
     c
     [{:playlist/id "pl"}
      {:song/id "song"}])
    (d/transact!
     c
     [{:playlist/id "pl"
       :db/id -1
       :playlist/date "date"}
      {:playlist-song/song [:song/id "song"]
       :playlist-song/playlist -1
       :playlist-song/attr 2028}])))

(comment
  ;; Switch to another database
  (def turso-store (create-storage {:remote? true :type :turso}))
  (def turso-conn (d/create-conn schema {:storage turso-store}))
  (-migrate-databases conn turso-conn))
