(ns app.ops.recipes
  (:require [app.model.mock-database :as db]
            [app.model.config :as cfg]
            [app.server-components.spotify :as sp]
            [datascript.core :as d]
            [taoensso.timbre :as log]
            [steffan-westcott.clj-otel.api.trace.span :as clj-span]))

(defn save-playlist-tracks!
  "Saves the tracks in the given list of playlists.
  Playlists are passed by name. If no playlists are passed, it saves the songs for all playlists recorded for the given user."
  ([{:keys [user connection] :as payload}]
   {:pre [(some? user)]}
   (clj-span/with-span! ["Save playlist tracks" {:user user}]
     (let [result (sp/get-recorded-playlists @connection user)
           playlist-names (map second result)]
       (save-playlist-tracks! payload playlist-names))))
  ([{:keys [user] :as payload} playlist-ids]
   (doseq [playlist playlist-ids]
     (clj-span/with-span! ["Save playlist tracks" {:user user :playlist-name playlist}]
       (sp/save-tracks! (merge payload {:playlist-name playlist}))))))

(defn -compute-latest-songs
  [db owner playlist-names n]
  (let [songs-per-playlist (map #(sp/get-playlist-songs db owner %) playlist-names)]
    (->> (map reverse songs-per-playlist)
         (flatten)
         (distinct)
         (take n)
         (reverse))))

(defn -collect-enough-songs!
  [connection user time-playlists n]
  (loop [[next-playlist & playlists] time-playlists
         seen-playlists []]
    (save-playlist-tracks! {:user user :connection connection}
                           [next-playlist])
    (let [seen-playlists (conj seen-playlists next-playlist)
          songs (-compute-latest-songs @connection user seen-playlists n)]
      (cond
        (empty? playlists) ; end of the iteration
        songs
        (= n (count songs)) ; enough songs collected
        songs
        :else ; look at the next playlist
        (recur playlists seen-playlists)))))

(defn create-latest-tracks!
  "Creates a playlist with the n latest tracks from the user's history playlists."
  [{:keys [connection user n] :as payload}]
  (clj-span/with-span! ["Create latest tracks" {:user user :n n}]
    (let [time-playlists (cfg/get-time-playlists @connection user)
          song-uris (-collect-enough-songs! connection user time-playlists n)
          {playlist-name :name} (sp/create-auto-playlist!
                                 (assoc payload :name (str "Latest " n " songs")))]
      (sp/empty-playlist! (assoc payload :playlist-name playlist-name))
      (sp/append-to-playlist! (merge payload
                                     {:playlist-name playlist-name
                                      :song-ids song-uris}))
      nil)))

(defn create-random-lib-playlist
  "Creates a playlist containing all songs from user's library in a random order.
  This fn does not clean any existing playlist. If the playlist already contains elements, this is a no-op."
  [{:keys [connection user playlist-name]
    :as payload
    :or {playlist-name "La duree"}}]
  (clj-span/with-span! ["Create random playlist" {:user user :playlist-name playlist-name}]
    (let [playlist-id (sp/get-playlist-id @connection user playlist-name)
          track-count (:playlist/track-count (d/entity @connection [:playlist/id playlist-id]))]
      (if (zero? track-count)
        (let [token (sp/get-valid-token! connection user)
              _ (log/debugf "Start fetching library of %s" user)
              tracks (sp/collect-library! token)
              _ (log/infof "Library fetched for %s" user)
              track-uris (map :uri tracks)]
          (log/debugf "Start feeding playlist %s" playlist-name)
          (sp/append-to-playlist!
           (assoc payload
                  :playlist-name playlist-name
                  :song-uris (shuffle track-uris)))
          (log/infof "Playlist %s fed succesfully" playlist-name)
          :filled)
        :not-empty))))

(defn -mix-history-playlists
  "Select at most `n` songs from the playlists.
  This must make sure that it does not omit songs if some playlists are shorter."
  [songs-per-playlists n]
  (let [random-tracks-per-playlist (map #(take n (concat (shuffle %)
                                                         (repeat nil)))
                                        songs-per-playlists)]
    (->> (reverse random-tracks-per-playlist)
         (apply interleave)
         (distinct)
         (remove nil?))))

(defn -pick-bits-of-history-tracks

  [db user n]
  (let [playlist-names (cfg/get-time-playlists db user)
        tracks-per-playlist (map
                             #(sp/get-playlist-songs db user %)
                             playlist-names)
        valid-playlists (remove nil? tracks-per-playlist)]
    (-mix-history-playlists valid-playlists n)))

(comment
  (-mix-history-playlists
   [(range 0 5)
    (range 10 13)
    (range 20 22)]
   5))

(defn create-bits-of-history-playlist!
  "Creates a playlist containing n tracks from each of the historical playlists.
  Tracks are inserted from the oldest playlist to the newest, interleaving them.
  This expects to find the playlists in the database."
  [{:keys [connection user n] :as payload :or {n 5}}]
  (clj-span/with-span! ["Create bits of history" {:user user :n n}]
    (let [track-ids (-pick-bits-of-history-tracks @connection user n)
          create-result (sp/create-auto-playlist!
                         (assoc payload :name "Bits of history"))
          {playlist-name :name :keys [existing?]} create-result]
      (when existing?
        (sp/empty-playlist!
         (assoc payload :playlist-name playlist-name)))
      (sp/append-to-playlist!
       (assoc payload
              :playlist-name playlist-name
              :song-ids track-ids)))))

(defn -pick-latest-songs-in-history
  "Picks n songs from the end of every history playlist for the given user and
  combine them into a single playlist."
  [db user n]
  (let [playlist-names (cfg/get-time-playlists db user)
        tracks-per-playlist (map
                             #(sp/get-playlist-songs db user %)
                             playlist-names)]
    (->> tracks-per-playlist
         (map #(take-last n %))
         (map reverse)
         (flatten)
         (distinct))))

(defn create-latest-in-history-playlist!
  "Creates a playlist with the n latest songs for history playlists."
  [{:keys [connection user n name] :as payload :or {n 5}}]
  (clj-span/with-span! ["Create lastest in history" {:user user :n n :playlist-name name}]
    (let [track-ids (-pick-latest-songs-in-history @connection user n)
          create-result (sp/create-auto-playlist!
                         (assoc payload :name name))
          {playlist-name :name :keys [existing?]} create-result]
      (when existing?
        (sp/empty-playlist!
         (assoc payload :playlist-name playlist-name)))
      (sp/append-to-playlist!
       (assoc payload
              :playlist-name playlist-name
              :song-ids track-ids)))))

(defn create-history-playlist!
  "Creates a playlist containing all songs from the history playlists in chronological order."
  [{:keys [connection user name] :as payload :or {name "Walking the Time"}}]
  (clj-span/with-span! ["Create history playlist" {:user user :playlist-name name}]
    (let [playlist-names (cfg/get-time-playlists @connection user)
          tracks-per-playlist (map
                               #(sp/get-playlist-songs @connection user %)
                               playlist-names)
          track-ids (-> tracks-per-playlist reverse flatten)
          create-result (sp/create-auto-playlist!
                         (assoc payload :name name))
          {playlist-name :name :keys [existing?]} create-result]
      (when existing?
        (sp/empty-playlist!
         (assoc payload :playlist-name playlist-name)))
      (sp/append-to-playlist!
       (assoc payload
              :playlist-name playlist-name
              :song-ids track-ids)))))

(comment
  (-pick-bits-of-history-tracks
   @db/conn
   db/colombe
   5))
