(ns app.ops.actions
  (:require
   [mount.core :as mount :refer [defstate]]
   [app.model.mock-database :as db]
   [app.model.config :as cfg]
   [app.server-components.spotify :as sp]
   [app.ops.recipes :as r]))

(defn -create-empty-report
  []
  (atom {:running? false}))

(defstate reports :start (-create-empty-report))

(defn sync-all-playlists!
  "Downloads the definitions of all playlists for all users"
  [connection]
  (doseq [user (cfg/get-users @connection)]
    (sp/save-playlists! {:connection connection
                         :user user})))

(defn run-task
  [f report-fn]
  (report-fn :in-progress)
  (try
    (f)
    (report-fn :success)
    (catch Exception _
      (report-fn :failed))))

(defn -create-report-fn
  [state tag]
  #(swap! state assoc-in [:progress tag] %))

(defn execute-in-scope!
  [& fns]
  (with-open [scope (new java.util.concurrent.StructuredTaskScope)]
    (doseq [f fns]
      (.fork scope f))
    (.join scope)))

(comment
  (let [a (atom {})]
    (with-open [scope (new java.util.concurrent.StructuredTaskScope)]
      (.fork scope #(swap! a conj (.getId (Thread/currentThread))))
      (.fork scope #(swap! a conj (.getId (Thread/currentThread))))
      (.join scope))
    @a)
  (let [a (atom {})]
    (execute-in-scope!
     #(run-task
       (partial println "hello 1")
       (-create-report-fn a :task-1))
     #(run-task
       (partial println "hello 2")
       (-create-report-fn a :task-2)))
    @a))

(defn run-all-updates!
  [{:keys [reports connection]}]
  (let [run (fn [tag f] (run-task f (-create-report-fn reports tag)))
        co-latest-tracks #(r/create-latest-tracks!
                           {:connection connection
                            :user db/colombe
                            :n 20})
        oli-latest-tracks #(r/create-latest-tracks!
                            {:connection connection
                             :user db/olivier
                             :n 20})
        co-history-bits #(r/create-bits-of-history-playlist!
                          {:connection connection
                           :user db/colombe})
        oli-history-bits #(r/create-bits-of-history-playlist!
                           {:connection connection
                            :user db/olivier})
        co-latest-history #(r/create-latest-in-history-playlist!
                            {:connection connection
                             :user db/colombe
                             :n 5
                             :name "Last songs"})]
    (run
     :sync-all-playlists
     #(sync-all-playlists! connection))
    (execute-in-scope!
     #(run :latest-tracks-for-co co-latest-tracks)
     #(run :latest-tracks-for-oli oli-latest-tracks)
     #(run :history-bits-for-co co-history-bits)
     #(run :history-bits-for-oli oli-history-bits)
     #(run :latest-history-for-co co-latest-history))))

(defn -flag-execution
  [state]
  (if-not (:running? state)
    {:running? true}
    state))

(defn -flag-completion
  [state]
  (assoc state
         :running? false
         :timestamp (java.time.LocalDateTime/now)))

(defn -start-vthread
  [f]
  (-> (Thread/ofVirtual)
      (.start f)))

(defn execute-and-update!
  [{:keys [reports] :as env}]
  (let [[old _] (swap-vals! reports -flag-execution)]
    (if-not (:running? old)
      (do
        (println "starting execution")
        (-start-vthread
         #(try
            (run-all-updates! env)
            (swap! reports -flag-completion)
            (swap! reports assoc :status :success)
            (catch Exception e
              (.printStackTrace e)
              (swap! reports :status :failed))
            (finally (swap! reports -flag-completion))))
        :executing)
      :in-progress)))

(comment
  (swap-vals! reports -flag-execution)
  (swap! reports -flag-completion)
  (execute-and-update! {:reports reports})
  (let [f (fn [k v] ((-create-report-fn reports k) v))]
    (f :a 1)
    (f :b 2)
    @reports))
