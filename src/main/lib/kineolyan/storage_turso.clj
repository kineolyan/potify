(ns lib.kineolyan.storage-turso
  (:require [clojure.string :as str]
            [clojure.edn :as edn]
            [clojure.spec.alpha :as s]
            [datascript.storage]
            [clj-http.client :as client]
            [datascript.core :as d]
            [cheshire.core :as json]))

;;; Specs

(s/def ::table-name (s/and string? (complement str/blank?)))
(s/def ::url (s/and string? (complement str/blank?)))
(s/def ::token (s/and string? (complement str/blank?)))

(s/def ::storage-options
  (s/keys :req-un [::table-name ::url ::token]))

(comment
  (s/explain ::storage-options {:table-name "abc"
                                :url "url"
                                :token "nekot"}))

;;; Methods

(defn -process-url
  [value]
  (str/replace-first value #"^libsql://" "https://"))

(comment
  (def turso-url (System/getenv "TURSO_URL"))
  (def turso-token (System/getenv "TURSO_TOKEN"))
  (def opts {:url (-process-url turso-url)
             :token turso-token}))

(defn compute-type
  [value]
  (cond
    (integer? value) :integer
    (float? value) :float
    (string? value) :text))

(defn prepare-argument
  [k v]
  {:name (name k)
   :value {:type (compute-type v)
           :value (str v)}})

(defn prepare-statement
  [{:keys [sql args]}]
  {:sql sql
   "named_args" (map #(apply prepare-argument %) args)})

(defn -call-turso!
  "Sends a call to Turso through the HTTP remote API."
  [{:keys [url token]} api-url payload]
  (let [post-options {:body (json/generate-string payload)
                      :headers {"Authorization" (str "Bearer " token)}
                      :content-type :json
                      :accept :json
                      :as :json}]
    (client/post (str url api-url) post-options)))

(defn -execute!
  [opts statements]
  (let [requests (concat
                  (mapv
                   #(hash-map :type :execute
                              :stmt (prepare-statement %)) statements)
                  [{:type :close}])
        result (-call-turso! opts "/v2/pipeline" {:requests requests})]
    (tap> result)
    (->> (:body result)
         (:results)
         (drop-last)
         (map :response)
         (filter #(= "execute" (:type %)))
         (map :result))))

(comment
  (prepare-statement {:sql "SELECT * from $table"
                      :args {:table "potify"}})
  (prepare-statement {:sql "SELECT * from table"})
  (prepare-statement
   {:sql
    "insert into axc (addr, content) values ($addr, $content) on conflict (addr) do update set content = $content;",
    :args {:addr 1000004, :content "{:level 0, :keys []}"}})
  (-execute! opts
             [{:sql
               "insert into axc (addr, content) values ($addr, $content) on conflict (addr) do update set content = $content;",
               :args {:addr 1000004, :content "{:level 0, :keys []}"}}])
  (-execute! opts [{:sql "SELECT * from axc"}]))

(defn -create-statement
  [{:keys [table-name]}]
  {:sql (format "CREATE TABLE IF NOT EXISTS
           %s (addr INTEGER PRIMARY KEY, content TEXT)"
                table-name)})

(defn -create-table!
  "Creates the underlying table if needed."
  [opts]
  (let [stmt (-create-statement opts)]
    (-execute! opts [stmt])))

(comment
  (-create-statement {:table-name "testing"})
  (-create-table! {:table-name "testing"}))

(defn -save-addr-statement
  [{:keys [table-name]} [addr data]]
  {:sql (format "insert into %s (addr, content)
                values ($addr, $content)
                on conflict (addr) do update set content = $content"
                table-name)
   :args {:addr addr
          :content (pr-str data)}})

(defn -store-impl
  [opts addr+data-seq]
  (let [updates (map (partial -save-addr-statement opts) addr+data-seq)
        result (-execute! opts updates)]
    (tap> [:store result])))

(defn -get-statement
  [{:keys [table-name]} addr]
  {:sql (format "select content from %s where addr = $addr" table-name)
   :args {:addr addr}})

(defn -value->clj
  [{:keys [type value] :as cell}]
  (case type
    "null" nil
    "integer" (Long/parseLong value)
    "float" (Double/parseDouble value)
    "text" value
    "blob" (throw (ex-info "Unsupported blob type" cell))))

(defn -rows->clj
  [rows]
  (mapv #(mapv -value->clj %) rows))

(defn -restore-impl
  [opts addr]
  (let [statement (-get-statement opts addr)
        result (-execute! opts [statement])]
    (tap> [:restore result])
    (->> (first result)
         :rows
         -rows->clj
         ffirst
         edn/read-string)))

(defn -list-addrs-impl
  [{:keys [table-name] :as opts}]
  (let [statement {:sql (format "select addr from %s" table-name)}
        result (-execute! opts [statement])]
    (->> (first result)
         :rows
         (map first))))

(defn -delete-impl
  [_opts _addrs-seq]
  ; TODO in the future
  nil)

(defn create-storage
  "Creates a Datascript storage, backed by Turso."
  [opts]
  {:pre [(s/valid? ::storage-options opts)]}
  (let [opts (update opts :url -process-url)]
    (-create-table! opts)
    (with-meta
      (reify datascript.storage/IStorage
        (-store [_ addr+data-seq]
          (-store-impl opts addr+data-seq))
        (-restore [_ addr]
          (-restore-impl opts addr))
        (-list-addresses [_]
          "Return seq that lists all addresses currently stored in your storage.
   Will be used during GC to remove keys that are no longer used."
          (-list-addrs-impl opts))
        (-delete [_ addrs-seq]
          "Delete data stored under `addrs` (seq). Will be called during GC"
          (-delete-impl opts addrs-seq)))
      {:options opts})))

(defn env->options
  "Build the option map to create a Turso storage from environment vars.
  It uses:
   - `TURSO_URL` to retrieve the URL of the database
   - `TURSO_TOKEN` to obtain the token to connect
   - `TURSO_TABLE` to obtain the taŕget table name"
  []
  {:url (System/getenv "TURSO_URL")
   :token (System/getenv "TURSO_TOKEN")
   :table-name (System/getenv "TURSO_TABLE")})

(comment
  (def rest-storage (create-storage {:table-name "axc"
                                     :url turso-url
                                     :token turso-token}))
  (meta rest-storage)

  (def rest-conn
    (d/create-conn {} {:storage rest-storage}))

  (import '[java.time Instant])
  ; insert data to see it react
  (d/transact!
   rest-conn
   [{:playlist/id "pl"}
    {:song/id "song"}])
  ; another transaction to see the new data
  (d/transact!
   rest-conn
   [{:playlist/id "pl-2"
     :playlist/name "my-playlist-second"
     :playlist/date (Instant/now)}])
  (d/transact!
   rest-conn
   [{:db/id 1
     :playlist/name "first-playlist"}])

  ;actions in the atom
  (d/datoms @rest-conn :eavt)
  (d/q
   '[:find ?value
     :where [_ _ ?value]]
   @rest-conn)

  (d/restore-conn rest-storage)

  (d/collect-garbage rest-conn))
