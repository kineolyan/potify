(ns user
  (:require [playback.preload]
            [clojure.pprint :as pp]
            [clojure.tools.namespace.repl :as tools-ns :refer [set-refresh-dirs]]
            [expound.alpha :as expound]
            [clojure.spec.alpha :as s]
            [datascript.core :as d]
            [app.model.mock-database :as db]
            [app.model.config :as cfg]
            [app.server-components.spotify :as sp]
            [app.ops.recipes :as r]))

(set-refresh-dirs "src/main" "src/dev" "src/test")
(alter-var-root #'s/*explain-out* (constantly expound/printer))

(comment
  (do
    ;; Setup the two users
    (d/transact!
     db/conn
     [#:account{:id db/olivier :real-name "Olivier"}
      #:account{:id db/colombe :real-name "Colombe"}])
    ;; Setup the default config for the two users
    (do
      (d/transact!
       db/conn
       [#:account{:id db/olivier
                  :spotify "kineolyan"}
        #:account{:id db/colombe
                  :spotify "chtitecerise"}])
      (d/transact!
       db/conn
       [#:config{:user [:account/id db/olivier]
                 :time-playlists (mapv str (range 2024 2014 -1))}
        #:config{:user [:account/id db/colombe]
                 :time-playlists ["Juin 2023"
                                  "Juin 2022"
                                  "Juin 2021"
                                  "Juin 2020"
                                  "juin 2019"
                                  "Juin 2018"
                                  "Juin 2017"
                                  "juin 2016"
                                  "Juin 2015"]}]))))

(comment
  ;; Save the logged user to the database
  (sp/save-token-to-db!
   db/conn
   db/olivier
   @sp/secret-db)
  (sp/save-token-to-db!
   db/conn
   db/colombe
   @sp/secret-db))

(defn sync-all-playlists!
  "Downloads the definitions of all playlists for all users"
  []
  (doseq [user [db/olivier db/colombe]]
    (sp/save-playlists! {:connection db/conn
                         :user user})))

(comment
  ;;; recipe execution
  (time (sync-all-playlists!))

  (time (r/create-latest-tracks!
         {:connection db/conn
          :user db/colombe
          :n 20}))
  (time (r/create-latest-tracks!
         {:connection db/conn
          :user db/olivier
          :n 20}))
  ;; empty if needed
  (time
   (sp/empty-playlist!
    {:connection db/conn
     :user db/olivier
     :playlist-name "[p] Latest 20 songs"}))
  (time
   (r/create-random-lib-playlist
    {:connection db/conn
     :user db/olivier
     :playlist-name "La duree"}))

  (time (r/create-bits-of-history-playlist!
         {:connection db/conn
          :user db/colombe}))
  (time (r/create-bits-of-history-playlist!
         {:connection db/conn
          :user db/olivier}))

  (time (r/create-latest-in-history-playlist!
         {:connection db/conn
          :user db/colombe
          :n 5
          :name "Last songs"}))

  (time (r/create-history-playlist!
         {:connection db/conn
          :user db/colombe
          :name "Juin 20xx"}))
  (time
   nil
    ; TODO for updates, it should be possible to only pass the list of playlists
    ; to add to the playlists.
    ; We remove all songs after the last stable playlist and add the songs from all
    ; playlists after.
    ; This assumes that playlists are stable after some time.
   #_(r/update-history-playlist!
      {:connection db/conn
       :user db/colombe
       :name "Juin 20xx"
       :last-playlist "Juin 2022"}))

  ;;;
  (r/save-playlist-tracks! {:connection db/conn
                            :user db/colombe})

  (let [res (d/q
             '[:find ?pid ?user ?name ?tracks
               :in $ [?user ...]
               :where
               [?uid :account/id ?user]
               [?pid :playlist/owner ?uid]
               [?pid :playlist/track-count ?tracks]
               [?pid :playlist/name ?name]]
             @db/conn
             [db/olivier db/colombe])]
    (pp/print-table (map #(zipmap [:pid :user :name :tracks] %) res)))

  ;; double-check what happened
  (sp/get-playlist-id @db/conn db/colombe "[p] Latest 20 songs")

  (sp/get-playlist-id @db/conn db/olivier "La duree")
  (sp/get-recorded-playlists @db/conn db/colombe)
  (cfg/get-time-playlists @db/conn db/colombe))
